from utils.user_agent import user_agent
import requests
from bs4 import BeautifulSoup
import random
import pickle
import os


def get_text(url):
    try:
        header = {'user-agent': random.choice(user_agent)}
        r = requests.get(url, headers=header)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        return r.text
    except:
        print("爬取失败：url = " + url)
        exit(-1)


def save_attrs(text):
    soup = BeautifulSoup(text, 'html.parser')
    world = soup.find('script', attrs={'id': "getListByCountryTypeService2true"})
    world = world.string
    world = world.replace('\xa0', ' ')
    world = world.replace('\u3000', ' ')
    world = world.split('[')[1][0:-12]
    world = world.split('{"id')

    countries = {}
    id = 0
    for country in world:
        if country == '':
            continue
        countries[id] = '"id' + country
        id += 1
    if not os.path.exists('./obj'):
        os.mkdir('./obj')
    with open('./obj/countries.pkl', 'wb') as f:
        pickle.dump(countries, f, pickle.HIGHEST_PROTOCOL)


def main():
    url = "https://ncov.dxy.cn/ncovh5/view/pneumonia"
    text = get_text(url)
    save_attrs(text)


if __name__ == '__main__':
    main()
