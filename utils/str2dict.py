import pickle


def str2dict():
    with open('obj/countries.pkl', 'rb') as f:
        countries = pickle.load(f)
        for c_id in countries.keys():
            new_infos = {}
            infos = countries[c_id]
            infos = infos.split(',')
            i = 0
            while i < len(infos):
                info = infos[i]
                if info == '':
                    i += 1
                    continue
                new_info = info.split(':')
                index = new_info[0][1:-1]
                if index == "incrVo":
                    index = new_info[1][2:-1]
                    content = new_info[2]
                elif index == 'deadIncr' or index == "showRank":
                    content = new_info[1][:-1]
                elif index == 'statisticsData':
                    content = new_info[1] + ":" + new_info[2]
                # elif (c_id == 206 or c_id == 209) and index == 'countryFullName':
                #     content = new_info[1] + infos[i + 1]
                #     i += 1
                elif len(new_info) > 1:
                    content = new_info[1]
                    if content == '"Bonaire' or content == '"VirginIslands':
                        content = new_info[1] + infos[i + 1]
                        i += 1
                else:
                    content = ''
                new_infos[index] = content
                i += 1
            countries[c_id] = new_infos
        with open('obj/countries_new.pkl', 'wb') as ff:
            pickle.dump(countries, ff, pickle.HIGHEST_PROTOCOL)

