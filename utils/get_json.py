import pickle
from utils.user_agent import user_agent
import requests
import random
import os
from tqdm import tqdm


def get_text(url):
    try:
        header = {'user-agent': random.choice(user_agent)}
        r = requests.get(url, headers=header)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
        # if r.encoding == "ISO-8859-1":
        #     r.encoding = r.apparent_encoding
        return r.text
    except:
        print("爬取失败：url = " + url)
        exit(-1)

def get_country_names():
    with open('obj/countries_new.pkl', 'rb') as f:
        countries = pickle.load(f)
        country_name = {}
        num = len(countries)  
        for i in tqdm(range(num)):
            country_name[i] = [countries[i]['countryShortCode'], countries[i]['countryFullName']]
        
        return num, country_name

def get_history():
    if not os.path.exists("obj"):
            os.mkdir("obj")

    with open('obj/countries_new.pkl', 'rb') as f:
        countries = pickle.load(f)
        num = len(countries)   

        if not os.path.exists("json"):
            os.mkdir("json")
            
        for i in tqdm(range(num)):
            url = countries[i]['statisticsData'][1:-1]
            history_data = get_text(url)

            with open('json/{}.json'.format(i), 'w') as json:
                json.writelines(history_data)


if __name__ == '__main__':
    get_history()
