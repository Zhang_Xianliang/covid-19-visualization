#!/usr/bin/python
# -*- coding: utf-8 -*-
# Author: Zhang Xianliang
# Date: From 2020/7/20
# Version: 1.0

from flask import Flask, render_template, Response
from flask_bootstrap import Bootstrap
from datetime import timedelta, datetime
import json
import pygal
from pymysql import *
from info_update import update_data_hour, update_data_day
import time
from country_dict import country_dict
import numpy as np


app = Flask(__name__)
Bootstrap(app)


@app.route('/')
def index():
    conn = connect(host='localhost', port=3306, user='root', password='48206468'
                    , database='coronavirus', charset='utf8')
    cursor = conn.cursor()
    sql = "select sum(confirmedCount), sum(curedCount), sum(deadCount), sum(currentConfirmedCount) from today;"
    num = cursor.execute(sql)
    results = cursor.fetchall()[0]
    confirmedCount = int(results[0])
    curedCount = int(results[1])
    deadCount = int(results[2])
    currentConfirmedCount = int(results[3])

    cursor.close()
    conn.close()

    curr_time = datetime.now()
    curr_time = curr_time.strftime('%Y-%m-%d %H:00:00')

    return render_template('index.html', title='COVID-19 Visulization',  # refresh_time=REFRESH_BROWSER,
    confirmedCount=confirmedCount, curedCount=curedCount, deadCount=deadCount, currentConfirmedCount=currentConfirmedCount,
    time=curr_time)


@app.route('/world_chart')
def world_chart():
    
    worldmap_chart = pygal.maps.world.World()
    worldmap_chart.title = 'Data in Map'

    conn = connect(host='localhost', port=3306, user='root', password='48206468'
                    , database='coronavirus', charset='utf8')
    cursor = conn.cursor()

    sql = '''select c.countryFullName, t.confirmedCount from today as t
        inner join country as c
        on c.id=t.country_id'''
    num = cursor.execute(sql)

    results = cursor.fetchall()

    country_less_1000 = {}
    country_less_10000 = {}
    country_less_100000 = {}
    country_less_1000000 = {}
    country_less_10000000 = {}
    country_less_100000000 = {}

    for name, count in results:
        if country_dict.get(name) is not None:
            if count < 1000:
                country_less_1000[country_dict[name]]  = count
            elif count < 10000:
                country_less_10000[country_dict[name]]  = count
            elif count < 100000:
                country_less_100000[country_dict[name]]  = count
            elif count < 1000000:
                country_less_1000000[country_dict[name]]  = count
            elif count < 10000000:
                country_less_10000000[country_dict[name]]  = count
            else:
                country_less_100000000[country_dict[name]]  = count
            
    worldmap_chart.add('<1000', country_less_1000)
    worldmap_chart.add('<10000', country_less_10000)
    worldmap_chart.add('<100000', country_less_100000)
    worldmap_chart.add('<1000000', country_less_1000000)
    worldmap_chart.add('<10000000', country_less_10000000)
    worldmap_chart.add('<100000000', country_less_100000000)
    cursor.close()
    conn.close()
    return Response(response=worldmap_chart.render(), content_type='image/svg+xml')


@app.route('/world_curve')
def world_curve():
    
    line_chart = pygal.Line(x_label_rotation=20)
    line_chart.title = 'Data in Curve Chart'

    conn = connect(host='localhost', port=3306, user='root', password='48206468'
                    , database='coronavirus', charset='utf8')
    cursor = conn.cursor()

    sql = '''select sum(confirmedCount), sum(currentConfirmedCount), sum(curedCount), sum(deadCount), dateID 
    from history_data group by dateID order by dateID'''

    cursor.execute(sql)

    results = cursor.fetchall()
    results = np.array(results)
    cursor.close()
    conn.close()

    confirmeds = results[:,0]
    currents = results[:,1]
    cureds = results[:,2]
    deads = results[:,3]
    dates = results[:,4]
    for i in range(len(dates)):
        if i % 10 == 0:
            dates[i] = dates[i].strftime('%Y-%m-%d')
        else:
            dates[i] = " "
    
    cureds[183] = 7966381
    currents[183] = 6054239

    line_chart.x_labels = dates
    line_chart.add("confirmed", confirmeds)
    line_chart.add("current", currents)
    line_chart.add("cured", cureds)
    line_chart.add("dead", deads)


    return Response(response=line_chart.render(), content_type='image/svg+xml')


@app.route('/detailed')
def detailed():
    conn = connect(host='localhost', port=3306, user='root', password='48206468'
                    , database='coronavirus', charset='utf8')
    cursor = conn.cursor()
    sql = "select sum(confirmedCount), sum(curedCount), sum(deadCount), sum(currentConfirmedCount) from today;"
    num = cursor.execute(sql)
    results = cursor.fetchall()[0]
    confirmedCount = int(results[0])
    curedCount = int(results[1])
    deadCount = int(results[2])
    currentConfirmedCount = int(results[3])

    sql = """select c.countryFullName, t.confirmedIncr, t.confirmedCount, t.currentConfirmedCount,
            t.curedIncr, t.curedCount, t.deadIncr, t.deadCount, t.deadRate from today as t 
            inner join country as c 
            on t.country_id=c.id order by t.confirmedCount desc;"""
    num = cursor.execute(sql)
    results = cursor.fetchall()
    datas = ""
    for i, result in enumerate(results):
        datas += '{{"countryFullName": "{}", "confirmedIncr": {}, "confirmedCount": {}, "currentConfirmedCount": {}, "curedIncr": {}, "curedCount":{}, "deadIncr": {}, "deadCount": {}, "deadRate": {}}}'.format(result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7], result[8])
        if i != len(results) - 1:
            datas += ','
    datas = '[' + datas + ']'

    cursor.close()
    conn.close()

    curr_time = datetime.now()
    curr_time = curr_time.strftime('%Y-%m-%d %H:00:00')
    return render_template('detailed.html', title='COVID-19 Visulization',  # refresh_time=REFRESH_BROWSER,
    confirmedCount=confirmedCount, curedCount=curedCount, deadCount=deadCount, currentConfirmedCount=currentConfirmedCount,
    time=curr_time, datas=datas)



@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500


if __name__ == '__main__':

    app.run()
