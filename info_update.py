from utils.spider import get_text, save_attrs
from utils.user_agent import user_agent
from utils.str2dict import str2dict
from utils.get_json import get_history, get_country_names
import pickle
from tqdm import tqdm
from pymysql import *
import json
import datetime
import time


class coronavirus:
    def __init__(self):
        print("---------------Connet database---------------")
        self.conn = connect(host='localhost', port=3306, user='root', password='48206468'
                       , database='coronavirus', charset='utf8')
        self.cursor = self.conn.cursor()
        print("Success!")

        print("---------------Initializate the System---------------")
        self.download_data()

        print("--------Download each country's history data----------")
        get_history()
        print("Success!")

        print("--------Get the number and names of countries----------")
        self.country_count, self.countriy_names = get_country_names()
        print("{} countries in total.".format(self.country_count))

        print("------Initialize Database------")
        self.init_database()
        print("Success!")

        print("------Insert country data------")
        self.init_country_data()

        print("------Insert today's data------")
        self.init_today_data()

        print("------Insert history data------")
        self.init_history_data()

    
    def download_data(self):
        print("---------------Download today's data-----------------")        
        url = "https://ncov.dxy.cn/ncovh5/view/pneumonia"
        text = get_text(url)

        print("Save today's data as ./obj/countries.pkl")
        save_attrs(text)
        print("Success!")

        print("---------Transfer data from string to dict------------")
        str2dict()
        print("Saved as ./obj/countries_new.pkl")

    def __del__(self):
        self.cursor.close()
        self.conn.close()

    def init_today_data(self):
        # sql = "alter table country auto_increment=1;"
        # self.cursor.execute(sql)
        # self.conn.commit()

        num = 0
        with open('obj/countries_new.pkl', 'rb') as f:
            countries = pickle.load(f)
            for i in tqdm(range(self.country_count)):
                num += 1
                sql = "insert into today values(0, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {})".format(
                    int(countries[i]['confirmedCount']), int(countries[i]['confirmedIncr']),
                    int(countries[i]['curedCount']), int(countries[i]['curedIncr']),
                    int(countries[i]['currentConfirmedCount']), int(countries[i]['currentConfirmedIncr']), 
                    int(countries[i]['deadCount']), int(countries[i]['deadIncr']),
                    int(countries[i]['suspectedCount']), float(countries[i]['deadRate'][1:-1]),
                    countries[i]['statisticsData'], i + 1, 
                    )
                self.cursor.execute(sql)
            self.conn.commit()
        print("Insert {} lines.".format(num))

    def init_country_data(self):
        # sql = "alter table country auto_increment=1;"
        # self.cursor.execute(sql)
        # self.conn.commit()
        
        num = 0
        with open('obj/countries_new.pkl', 'rb') as f:
            countries = pickle.load(f)
            for i in tqdm(range(len(countries))):
                num += 1
                sql = "insert into country values(0, {}, {})".format(self.countriy_names[i][0], self.countriy_names[i][1])
 
                self.cursor.execute(sql)
            self.conn.commit()
        print("Insert {} lines.".format(num))

    def init_history_data(self):
        
        num = 0
        for i in tqdm(range(self.country_count)):
            with open("json/{}.json".format(i), 'r') as f:
                info_till_now = json.load(f)
                info_till_now = info_till_now['data']
                
                for info_one_day in info_till_now:
                    num += 1
                    sql = "insert into history_data values(0, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {})".format(
                    info_one_day['confirmedCount'], info_one_day['confirmedIncr'],
                    info_one_day['curedCount'], info_one_day['curedIncr'],
                    info_one_day['currentConfirmedCount'], info_one_day['currentConfirmedIncr'], 
                    info_one_day['deadCount'], info_one_day['deadIncr'],
                    info_one_day['suspectedCount'],
                    info_one_day['dateId'], i + 1, 
                    )
                    self.cursor.execute(sql)
                self.conn.commit()

        print("Insert {} lines.".format(num))

    def init_database(self):
        print("------Initialize table `country`------")
        sql = '''create table if not exists country(
            id tinyint unsigned not null auto_increment primary key,
            countryShortName varchar(50) not null,
            countryFullName varchar(100) not null
            );
        '''
        self.cursor.execute(sql)
        print("Success!")

        print("------Initialize table `today`------")
        sql = '''
            create table if not exists today(
            id int unsigned not null auto_increment primary key,
            confirmedCount int default 0,
            confirmedIncr int default 0,
            curedCount int default 0,
            curedIncr int default 0,
            currentConfirmedCount int default 0,
            currentConfirmedIncr int default 0,
            deadCount int default 0,
            deadIncr int default 0,
            suspectedCount int default 0,
            deadRate float(5,2) default 0.00,
            statisticsData varchar(100) not null,
            country_id tinyint unsigned not null,  

            FOREIGN KEY (country_id) REFERENCES country(id)
        );
        '''
        self.cursor.execute(sql)
        print("Success!")

        print("------Initialize table of each country's history data `history_data`------")
        sql = '''
            create table if not exists history_data(
            id int unsigned not null auto_increment primary key,
            confirmedCount int default 0,
            confirmedIncr int default 0,
            curedCount int default 0,
            curedIncr int default 0,
            currentConfirmedCount int default 0,
            currentConfirmedIncr int default 0,
            deadCount int default 0,
            deadIncr int default 0,
            suspectedCount int default 0,
            dateId date,
            country_id tinyint unsigned not null,  
            FOREIGN KEY (country_id) REFERENCES country(id)
        );
        '''
        self.cursor.execute(sql)
        print("Success!")

    def update_data_hour(self): 
        self.download_data()

        num = 0
        print("Updating data in table today...")
        with open('obj/countries_new.pkl', 'rb') as f:
            countries = pickle.load(f)
            for i in tqdm(range(self.country_count)):
                num += 1
                sql = '''update today set confirmedCount={}, confirmedIncr={}, curedCount={},
                        curedIncr={}, currentConfirmedCount={}, currentConfirmedIncr={},
                        deadCount={}, deadIncr={}, suspectedCount={}, deadRate={} where country_id=(select id from country where countryFullName={})'''.format(
                    int(countries[i]['confirmedCount']), int(countries[i]['confirmedIncr']),
                    int(countries[i]['curedCount']), int(countries[i]['curedIncr']),
                    int(countries[i]['currentConfirmedCount']), int(countries[i]['currentConfirmedIncr']), 
                    int(countries[i]['deadCount']), int(countries[i]['deadIncr']),
                    int(countries[i]['suspectedCount']), float(countries[i]['deadRate'][1:-1]),
                    countries[i]['countryFullName']
                    )
                self.cursor.execute(sql)
            self.conn.commit()
        print("Update {} lines in table today.".format(num))
  
    def update_data_day(self):
        self.download_data()

        num = 0
        date = datetime.date.today().strftime(r"%Y-%m-%d")
        
        print("Updating data in table today and history...")
        with open('obj/countries_new.pkl', 'rb') as f:
            countries = pickle.load(f)
            for i in tqdm(range(self.country_count)):
                num += 1
                sql = '''update today set confirmedCount={}, confirmedIncr={}, curedCount={},
                        curedIncr={}, currentConfirmedCount={}, currentConfirmedIncr={},
                        deadCount={}, deadIncr={}, suspectedCount={}, deadRate={}, statisticsData={}
                        where country_id=(select id from country where countryFullName={})'''.format(
                    int(countries[i]['confirmedCount']), int(countries[i]['confirmedIncr']),
                    int(countries[i]['curedCount']), int(countries[i]['curedIncr']),
                    int(countries[i]['currentConfirmedCount']), int(countries[i]['currentConfirmedIncr']), 
                    int(countries[i]['deadCount']), int(countries[i]['deadIncr']),
                    int(countries[i]['suspectedCount']), float(countries[i]['deadRate'][1:-1]),
                    countries[i]['statisticsData'], countries[i]['countryFullName']
                    )
                self.cursor.execute(sql)
                
                sql = "insert into history_data values(0, {}, {}, {}, {}, {}, {}, {}, {}, {}, '{}', (select id from country where countryFullName={}))".format(
                countries[i]['confirmedCount'], countries[i]['confirmedIncr'],
                countries[i]['curedCount'], countries[i]['curedIncr'],
                countries[i]['currentConfirmedCount'], countries[i]['currentConfirmedIncr'], 
                countries[i]['deadCount'], countries[i]['deadIncr'],
                countries[i]['suspectedCount'],
                date, countries[i]['countryFullName'], 
                )

                self.cursor.execute(sql)

                
            self.conn.commit()
        print("Update {} lines in table today.".format(num))
        print("Insert {} lines in table history_data.".format(num))

def download_data():
    print("---------------Download today's data-----------------")        
    url = "https://ncov.dxy.cn/ncovh5/view/pneumonia"
    text = get_text(url)

    print("Save today's data as ./obj/countries.pkl")
    save_attrs(text)
    print("Success!")

    print("---------Transfer data from string to dict------------")
    str2dict()
    print("Saved as ./obj/countries_new.pkl")

def update_data_hour(conn, cursor): 
    download_data()

    num = 0
    with open('obj/countries_new.pkl', 'rb') as f:
        countries = pickle.load(f)
        for i in tqdm(range(len(countries))):
            num += 1
            sql = '''update today set confirmedCount={}, confirmedIncr={}, curedCount={},
                    curedIncr={}, currentConfirmedCount={}, currentConfirmedIncr={},
                    deadCount={}, deadIncr={}, suspectedCount={}, deadRate={} where id={}'''.format(
                int(countries[i]['confirmedCount']), int(countries[i]['confirmedIncr']),
                int(countries[i]['curedCount']), int(countries[i]['curedIncr']),
                int(countries[i]['currentConfirmedCount']), int(countries[i]['currentConfirmedIncr']), 
                int(countries[i]['deadCount']), int(countries[i]['deadIncr']),
                int(countries[i]['suspectedCount']), float(countries[i]['deadRate'][1:-1]),
                i + 1, 
                )
            cursor.execute(sql)
        conn.commit()
    print("Update {} lines in table today.".format(num))
  
def update_data_day(conn, cursor):
    download_data()

    num = 0
    date = datetime.date.today().strftime(r"%Y-%m-%d")
    with open('obj/countries_new.pkl', 'rb') as f:
        countries = pickle.load(f)
        for i in tqdm(range(len(countries))):
            num += 1
            sql = '''update today set confirmedCount={}, confirmedIncr={}, curedCount={},
                    curedIncr={}, currentConfirmedCount={}, currentConfirmedIncr={},
                    deadCount={}, deadIncr={}, suspectedCount={}, deadRate={}, statisticsData={} where id={}'''.format(
                int(countries[i]['confirmedCount']), int(countries[i]['confirmedIncr']),
                int(countries[i]['curedCount']), int(countries[i]['curedIncr']),
                int(countries[i]['currentConfirmedCount']), int(countries[i]['currentConfirmedIncr']), 
                int(countries[i]['deadCount']), int(countries[i]['deadIncr']),
                int(countries[i]['suspectedCount']), float(countries[i]['deadRate'][1:-1]),
                countries[i]['statisticsData'], i + 1
                )
            cursor.execute(sql)
            
            sql = "insert into history_data values(0, {}, {}, {}, {}, {}, {}, {}, {}, {}, '{}', {})".format(
            countries[i]['confirmedCount'], countries[i]['confirmedIncr'],
            countries[i]['curedCount'], countries[i]['curedIncr'],
            countries[i]['currentConfirmedCount'], countries[i]['currentConfirmedIncr'], 
            countries[i]['deadCount'], countries[i]['deadIncr'],
            countries[i]['suspectedCount'],
            date, i + 1, 
            )

            cursor.execute(sql)

        conn.commit()
    print("Update {} lines in table today.".format(num))
    print("Insert {} lines in table history_data.".format(num))


def main():
    corona = coronavirus()
    last_hour = -1
    while True:
        date = datetime.datetime.now()
        print("Now: {}".format(date))
        hour = date.hour
        if hour != last_hour:
            try:
                if hour == 23:
                    corona.update_data_day()
                else:
                    corona.update_data_hour()
                last_hour = hour
            except:
                pass
        time.sleep(1000)


if __name__ == "__main__":
    main()
